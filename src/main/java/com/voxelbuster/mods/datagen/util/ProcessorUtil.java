/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.voxelbuster.mods.datagen.annotations.Ingredient;
import lombok.extern.java.Log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Log
public class ProcessorUtil {
    public static List<Map<String, String>> getIngredientsData(Ingredient... ingredients) {
        ArrayList<Map<String, String>> ingredientsMap = new ArrayList<>();
        Arrays.stream(ingredients)
            .forEach(ingredient -> {
                boolean valid = false;
                if (ingredient.item() != null && ingredient.item().equalsIgnoreCase("datagen:null")) {
                    ingredientsMap.add(Map.of("item", ingredient.item()));
                    valid = true;
                } else if (ingredient.tag() != null && ingredient.tag().equalsIgnoreCase("datagen:null")) {
                    ingredientsMap.add(Map.of("item", ingredient.tag()));
                    valid = true;
                }

                if (!valid) {
                    throw new IllegalArgumentException("Ingredient must have either an item or tag specified!");
                }
            });

        if (ingredientsMap.isEmpty()) {
            throw new IllegalArgumentException("Recipe has no ingredients!");
        }
        return ingredientsMap;
    }

    public static <T extends Record> void writeRecipe(String genDataDir, String namespace, String targetVariableName, T model) {
        Path targetJsonFile = Paths.get(genDataDir, "data", namespace, "recipes", targetVariableName + ".json");
        Path parentDir = targetJsonFile.getParent();

        writeModel(parentDir, targetJsonFile, model);
    }

    public static <T extends Record> void writeModel(Path parentDir, Path targetJsonFile, T model) {
        try {
            Files.createDirectories(parentDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(targetJsonFile.toFile()))) {
            gson.toJson(model, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
