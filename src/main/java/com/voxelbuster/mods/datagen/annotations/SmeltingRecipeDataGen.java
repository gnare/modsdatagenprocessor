package com.voxelbuster.mods.datagen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD, ElementType.LOCAL_VARIABLE})
public @interface SmeltingRecipeDataGen {
    String type() default "minecraft:smelting";

    String category() default "misc";

    int cookingTime() default 200;

    double experience() default 0.1;

    Ingredient[] ingredients();

    String result() default "datagen:self";
}