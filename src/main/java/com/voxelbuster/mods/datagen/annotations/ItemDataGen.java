/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.annotations;

import com.voxelbuster.mods.datagen.annotations.types.EnumTextureSlot;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD, ElementType.LOCAL_VARIABLE})
public @interface ItemDataGen {

    String en_us() default "datagen.item.null.name";

    String texturePrefix() default "item/";

    TextureEntry[] textures() default {@TextureEntry("null_item_texture")};

    String modelParent() default "minecraft:item/generated";

    boolean useDefaultBlockItemDisplay() default false;

    @Retention(RetentionPolicy.SOURCE)
    @interface TextureEntry {
        String value();

        EnumTextureSlot slot() default EnumTextureSlot.LAYER0;
    }
}
