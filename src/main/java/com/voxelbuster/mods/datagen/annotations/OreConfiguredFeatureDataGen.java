/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.annotations;

public @interface OreConfiguredFeatureDataGen {
    String type() default "minecraft:ore";
    int size() default 12;

    Target[] targets();

    @interface Target {
        String predicateType() default "minecraft:tag_match";

        String targetTag() default "minecraft:stone_ore_replaceables";

        String targetBlock() default "minecraft:stone";

        double targetBlockProb() default 0.5;
        String replacementState();

        String replacementStateProps() default "";
    }
}
