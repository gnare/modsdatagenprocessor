/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.annotations.types;

public enum EnumTextureSlot {
    ALL,
    TEXTURE,
    PARTICLE,
    END,
    BOTTOM,
    TOP,
    FRONT,
    BACK,
    SIDE,
    NORTH,
    SOUTH,
    EAST,
    WEST,
    UP,
    DOWN,
    CROSS,
    PLANT,
    WALL,
    RAIL,
    WOOL,
    PATTERN,
    PANE,
    EDGE,
    FAN,
    STEM,
    UPPER_STEM,
    CROP,
    DIRT,
    FIRE,
    LANTERN,
    PLATFORM,
    UNSTICKY,
    TORCH,
    LAYER0,
    LAYER1,
    LAYER2,
    LIT_LOG,
    CANDLE,
    INSIDE,
    CONTENT,
    INNER_TOP,
    FLOWERBED
}
