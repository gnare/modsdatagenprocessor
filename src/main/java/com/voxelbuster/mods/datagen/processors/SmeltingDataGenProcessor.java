/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.processors;

import com.google.auto.service.AutoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.voxelbuster.mods.datagen.annotations.DataGenNamespace;
import com.voxelbuster.mods.datagen.annotations.ItemDataGen;
import com.voxelbuster.mods.datagen.annotations.SmeltingRecipeDataGen;
import com.voxelbuster.mods.datagen.annotations.TagWith;
import com.voxelbuster.mods.datagen.model.SmeltingRecipeDataModel;
import com.voxelbuster.mods.datagen.util.ProcessorUtil;
import lombok.extern.java.Log;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Log
@AutoService(Processor.class)
public class SmeltingDataGenProcessor extends AbstractProcessor {
    private String genDataDir;

    @Override
    public Set<String> getSupportedOptions() {
        return Set.of("generatedDataDir");
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(ItemDataGen.class.getName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        this.genDataDir = processingEnv.getOptions().get("generatedDataDir");
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        roundEnv.getElementsAnnotatedWith(TagWith.class).forEach(
            annotatedElement -> {
                SmeltingRecipeDataGen annotation = annotatedElement.getAnnotation(SmeltingRecipeDataGen.class);

                Element enclosingElement = annotatedElement.getEnclosingElement();
                log.fine("Enclosing element " + enclosingElement.getSimpleName().toString());
                if (enclosingElement.getAnnotation(DataGenNamespace.class) == null) {
                    throw new RuntimeException(
                        "You must have a @DataGenNamespace annotation on the enclosing block of any @SmeltingRecipeDataGen!");
                }
                String namespace = enclosingElement.getAnnotation(DataGenNamespace.class).value();

                String targetVariableName = annotatedElement.getSimpleName().toString();
                String targetTypeName = annotatedElement.getClass().getName();

                log.info("Processing @SmeltingRecipeDataGen annotated variable %s:%s".formatted(targetVariableName, targetTypeName));

                List<Map<String, String>> ingredientsMap = ProcessorUtil.getIngredientsData(annotation.ingredients());

                SmeltingRecipeDataModel model = new SmeltingRecipeDataModel(
                    annotation.type(),
                    annotation.category(),
                    annotation.cookingTime(),
                    annotation.experience(),
                    ingredientsMap,
                    annotation.result().isEmpty() || annotation.result().equalsIgnoreCase("datagen:self") ?
                        namespace + ":" + targetVariableName :
                        annotation.result()
                );

                ProcessorUtil.writeRecipe(genDataDir, namespace, targetVariableName, model);
            });

        return true;
    }
}
