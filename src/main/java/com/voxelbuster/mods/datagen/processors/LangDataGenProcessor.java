/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.processors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.voxelbuster.mods.datagen.annotations.BlockDataGen;
import com.voxelbuster.mods.datagen.annotations.DataGenNamespace;
import com.voxelbuster.mods.datagen.annotations.ItemDataGen;
import com.voxelbuster.mods.datagen.model.LangDataModel;
import lombok.extern.java.Log;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Log
public class LangDataGenProcessor {
    private static final HashMap<String, LangDataModel> namespaceDataModelMap = new HashMap<>();

    public static boolean processLang(RoundEnvironment roundEnv, String genDataDir) {
        roundEnv.getElementsAnnotatedWithAny(Set.of(ItemDataGen.class, BlockDataGen.class))
            .forEach(annotatedElement -> {
                    Element enclosingElement = annotatedElement.getEnclosingElement();
                    if (enclosingElement.getAnnotation(DataGenNamespace.class) == null) {
                        throw new RuntimeException(
                            "You must have a @DataGenNamespace annotation on the enclosing block of any @*DataGen!");
                    }

                    String targetVariableName = annotatedElement.getSimpleName().toString();
                    String targetTypeName = annotatedElement.getClass().getName();

                    String namespace = enclosingElement.getAnnotation(DataGenNamespace.class).value();
                    if (!namespaceDataModelMap.containsKey(namespace)) {
                        namespaceDataModelMap.put(namespace, new LangDataModel());
                    }

                    ItemDataGen itemAnnotation = annotatedElement.getAnnotation(ItemDataGen.class);
                    BlockDataGen blockAnnotation = annotatedElement.getAnnotation(BlockDataGen.class);

                    if (Objects.nonNull(itemAnnotation)) {

                        log.info("Processing annotated variable for language data %s:%s".formatted(targetVariableName, targetTypeName));

                        String itemName = (!itemAnnotation.en_us().equals("datagen.item.null.name")) ?
                            itemAnnotation.en_us() :
                            convertCase(targetVariableName);

                        String langKey = "item.%s.%s".formatted(namespace, targetVariableName);

                        namespaceDataModelMap.get(namespace).put(langKey, itemName);
                    }

                    if (Objects.nonNull(blockAnnotation)) {

                        log.info("Processing annotated variable for language data %s:%s".formatted(targetVariableName, targetTypeName));

                        String blockName = (!blockAnnotation.en_us().equals("datagen.tile.null.name")) ?
                            blockAnnotation.en_us() :
                            convertCase(targetVariableName);

                        String langKey = "block.%s.%s".formatted(namespace, targetVariableName);

                        namespaceDataModelMap.get(namespace).put(langKey, blockName);
                    }

                }
            );

        for (String namespaceKey : namespaceDataModelMap.keySet()) {
            Path targetJsonFile = Paths.get(genDataDir, "assets", namespaceKey, "lang", "en_us.json");
            Path parentDir = targetJsonFile.getParent();

            try {
                Files.createDirectories(parentDir);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            LangDataModel langDataModel = namespaceDataModelMap.get(namespaceKey);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(targetJsonFile.toFile()))) {
                gson.toJson(langDataModel, writer);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return true;
    }

    @Contract(pure = true)
    public static @NotNull String convertCase(@NotNull String snakeOrCamelCase) {
        // Split snake_case
        return Arrays.stream(snakeOrCamelCase.split("[_\\s]"))
            // Split camelCase
            .map(input -> input.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])"))
            .flatMap(Arrays::stream)
            .map(String::toLowerCase)
            // Capitalize first letter of each
            .map(input -> input.substring(0, 1).toUpperCase() + input.substring(1))
            // Stick them all together with spaces
            .collect(Collectors.joining(" ")).trim();
    }
}
