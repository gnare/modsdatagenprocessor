/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.processors;

import com.google.auto.service.AutoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.voxelbuster.mods.datagen.annotations.BlockDataGen;
import com.voxelbuster.mods.datagen.annotations.DataGenNamespace;
import com.voxelbuster.mods.datagen.model.BlockDataModel;
import lombok.extern.java.Log;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

@Log
@AutoService(Processor.class)
public class BlockDataGenProcessor extends AbstractProcessor {
    private String genDataDir;

    @Override
    public Set<String> getSupportedOptions() {
        return Set.of("generatedDataDir");
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(BlockDataGen.class.getName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        this.genDataDir = processingEnv.getOptions().get("generatedDataDir");
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        roundEnv.getElementsAnnotatedWith(BlockDataGen.class).forEach(
            annotatedElement -> {
                BlockDataGen annotation = annotatedElement.getAnnotation(BlockDataGen.class);

                Element enclosingElement = annotatedElement.getEnclosingElement();
                if (enclosingElement.getAnnotation(DataGenNamespace.class) == null) {
                    throw new RuntimeException(
                        "You must have a @DataGenNamespace annotation on the enclosing block of any @BlockDataGen!");
                }

                String namespace = enclosingElement.getAnnotation(DataGenNamespace.class).value();

                String targetVariableName = annotatedElement.getSimpleName().toString();
                String targetTypeName = annotatedElement.getClass().getName();

                log.info("Processing @BlockDataGen annotated variable %s:%s".formatted(targetVariableName, targetTypeName));

                Map<String, String> textures;

                //noinspection unchecked
                textures =
                    Map.ofEntries(
                        Arrays.stream(annotation.textures())
                            .map(entry -> {
                                if (entry.value().equals("null_block_texture")) {
                                    return Map.entry(entry.slot().toString().toLowerCase(), "%s:%s%s".formatted(namespace, annotation.texturePrefix(), targetVariableName));
                                }

                                return Map.entry(entry.slot().toString().toLowerCase(), "%s:%s%s".formatted(namespace, annotation.texturePrefix(), entry.value()));
                            })
                            .toArray(size -> new Map.Entry[size]));

                Path targetJsonFile = Paths.get(genDataDir, "assets", namespace, "models", "block", targetVariableName + ".json");
                Path parentDir = targetJsonFile.getParent();

                try {
                    Files.createDirectories(parentDir);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                BlockDataModel itemDataModel = new BlockDataModel(annotation.modelTemplate(), textures);
                Gson gson = new GsonBuilder().setPrettyPrinting().create();

                try (BufferedWriter writer = new BufferedWriter(new FileWriter(targetJsonFile.toFile()))) {
                    gson.toJson(itemDataModel, writer);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

        LangDataGenProcessor.processLang(roundEnv, genDataDir);

        return true;
    }

    @Override
    public Iterable<? extends Completion> getCompletions(
        Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        return Collections.emptyList();
    }
}
