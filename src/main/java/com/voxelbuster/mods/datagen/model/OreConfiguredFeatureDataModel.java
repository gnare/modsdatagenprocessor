/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package com.voxelbuster.mods.datagen.model;

import java.util.List;
import java.util.Map;

public record OreConfiguredFeatureDataModel(String type, ConfigBlock config) {

    public record ConfigBlock(int size, double discard_chance_on_air_exposure, List<ConfigTarget> targets) {

    }

    public record ConfigTarget(Map<String, Object> target, Map<String, Object> state) {
        public static ConfigTarget defaultStoneTarget(String replacementStateName) {
            return new ConfigTarget(Map.of(
                    "predicate_type", "minecraft:tag_match",
                    "tag", "minecraft:stone_ore_replaceables"
                ),
                Map.of("Name", replacementStateName));
        }

        public static ConfigTarget defaultDeepslateTarget(String replacementStateName) {
            return new ConfigTarget(Map.of(
                "predicate_type", "minecraft:tag_match",
                "tag", "minecraft:deepslate_ore_replaceables"
            ),
                Map.of("Name", replacementStateName));
        }
    }
}
