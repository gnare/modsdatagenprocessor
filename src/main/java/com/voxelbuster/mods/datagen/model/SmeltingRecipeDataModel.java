package com.voxelbuster.mods.datagen.model;

import java.util.List;
import java.util.Map;

public record SmeltingRecipeDataModel(String type, String group, int cookingtime,
                                      double experience, List<Map<String, String>> ingredient, String result) {
}