module datagen {
    requires java.compiler;
    requires java.base;
    requires java.logging;

    requires org.jetbrains.annotations;
    requires com.google.gson;
    requires lombok;
    requires com.google.auto.service;

    exports com.voxelbuster.mods.datagen.annotations;
    exports com.voxelbuster.mods.datagen.annotations.container;
    exports com.voxelbuster.mods.datagen.annotations.types;
    exports com.voxelbuster.mods.datagen.model;
    exports com.voxelbuster.mods.datagen.processors;

    provides javax.annotation.processing.Processor with
        com.voxelbuster.mods.datagen.processors.ItemDataGenProcessor,
        com.voxelbuster.mods.datagen.processors.BlockDataGenProcessor,
        com.voxelbuster.mods.datagen.processors.ShapelessDataGenProcessor,
        com.voxelbuster.mods.datagen.processors.ShapedDataGenProcessor,
        com.voxelbuster.mods.datagen.processors.SmeltingDataGenProcessor,
        com.voxelbuster.mods.datagen.processors.SmithingDataGenProcessor,
        com.voxelbuster.mods.datagen.processors.TagsDataGenProcessor;
}